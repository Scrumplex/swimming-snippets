#!/usr/bin/env fish
for p in (yay -Qq)
    if test (pactree -r $p | head -n 2 | wc -l) = "1"
        echo $p
    end
end
